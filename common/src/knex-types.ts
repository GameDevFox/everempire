import Knex from 'knex';

export type DB = ReturnType<typeof Knex>;
