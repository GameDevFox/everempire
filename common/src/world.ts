export interface World {
  id: number;
  name: string;
  playerCount: number;
};

