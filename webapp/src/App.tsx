import axios from 'axios';
import _ from 'lodash';
import { useState } from 'react';
import { IconType } from 'react-icons';
import { BsPlayFill, BsPlusLg, BsTrashFill } from 'react-icons/bs';
import { useQuery, useQueryClient } from 'react-query';

import type { World } from '@everempire/common';
import {
  Flex, Table, Heading, Skeleton, Tbody,
  Thead, Tr, Th, Td, IconButton, Button, Box, Input
} from '@chakra-ui/react';

interface IconProps {
  type: IconType;
  size?: string;
}

const Icon = ({ type, size }: IconProps) => {
  const IconType = type;

  const sizeProps = size ? { style: { width: size, height: size }} : {};
  return <IconType {...sizeProps}/>;
};

const api = axios.create({
  baseURL: 'http://localhost:8080',
});

const App = () => {
  const queryClient = useQueryClient();
  const query = useQuery<World[]>('worlds', () => {
    return api.get('/world').then(res => res.data);
  });

  const [name, setName] = useState('');

  const {
    isLoading, isFetching, isSuccess, isError, data: worlds
  } = query;

  const skeletonCount = isLoading ? 6 : 3;

  const createWorld = async () => {
    if(name.trim() === '')
      return;

    await api.post('/world', { name });
    queryClient.invalidateQueries('worlds');

    setName('');
  };

  const deleteWorld = async (id: World['id']) => {
    await api.delete(`/world/${id}`);
    queryClient.invalidateQueries('worlds');
  };

  return (
    <Box>
      <Flex direction='row' alignItems='center' gap={2}>
        <Heading flexGrow={1} margin={3} marginBottom={0} marginRight={0}>
          Worlds
        </Heading>
        <Input w='200px' placeholder='Name' flexShrink={0}
          value={name} onChange={e => setName(e.currentTarget.value)}
        />
        <Button
          colorScheme='green' rightIcon={<BsPlusLg/>} marginRight={2} flexShrink={0}
          onClick={() => createWorld()}
        >
          Create
        </Button>
      </Flex>

      <Table>
        <Thead>
          <Tr>
            <Th>Name</Th>
            <Th>Player Count</Th>
            <Th>Actions</Th>
          </Tr>
        </Thead>

        <Tbody>
          {isError && (
            <Tr>
              <Td colSpan={3} color='red.500' fontWeight='bold'>
                Error: Failed to retrive worlds
              </Td>
            </Tr>
          )}

          {isSuccess && worlds.map(({ id, name, playerCount}) => (
            <Tr key={id}>
              <Td>{name}</Td>
              <Td>{playerCount}</Td>
              <Td>
                <IconButton
                  variant='ghost' colorScheme='green' aria-label='play'
                  icon={<Icon type={BsPlayFill} size='28px'/>}
                />
                <IconButton
                  onClick={() => deleteWorld(id)}
                  variant='ghost' colorScheme='red' aria-label='play'
                  icon={<Icon type={BsTrashFill} size='28px'/>}
                />
              </Td>
            </Tr>
          ))}

          {(isLoading || isFetching) && (
            _.range(skeletonCount).map(num => (
              <Tr key={num}>
                <Td><Skeleton>name</Skeleton></Td>
                <Td><Skeleton>count</Skeleton></Td>
                <Td><Skeleton>actions</Skeleton></Td>
              </Tr>
            ))
          )}
        </Tbody>
      </Table>
    </Box>
  );
};

export default App;
