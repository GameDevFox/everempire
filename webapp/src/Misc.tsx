import {
  Button, Kbd, Text, Wrap, WrapItem, Circle, Flex, Spacer, Box, Stack, Container,
  useBreakpoint, useColorMode
} from '@chakra-ui/react';


export const Misc = () => {
  const x = useBreakpoint();
  const { colorMode, toggleColorMode } = useColorMode();

  return (
    <Flex padding='10px' gap='10px' direction='column' height='400px'>
      <Circle size='50px' borderWidth='3px' fontWeight='bold'>
        EE
      </Circle>

      <Spacer/>

      <Container borderWidth='2px'>
        <Stack direction={{ lg: 'row', base: 'column', '2xl': 'column' }} spacing='24px'>
          <Box w='40px' h='40px' bg='yellow.200'>
            1
          </Box>
          <Box w='40px' h='40px' bg='tomato'>
            2
          </Box>
          <Box w='40px' h='40px' bg='pink.100'>
            3
          </Box>
        </Stack>
      </Container>

      <Container borderWidth='2px' maxWidth='800px'>
        <Text>
          Hello World {x}
        </Text>
      </Container>

      <Spacer/>

      <Wrap>
        <WrapItem>
          <Kbd>Your</Kbd> + <Kbd>Mom</Kbd>
          aosidjqoiw djqwiojd oqwjd ioqwj diowqj diowjq
        </WrapItem>
        <WrapItem className="here-it-is" dropShadow='dark-lg'>
          qowidjqw
          <Button onClick={toggleColorMode}>{colorMode} Theme</Button>
        </WrapItem>
      </Wrap>
    </Flex>
  );
};
