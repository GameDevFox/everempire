import { Knex } from "knex";

const TABLE_NAME = 'world';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable(TABLE_NAME, table => {
    table.increments();
    table.string('name');
    table.string('player_count');
    table.timestamps(false, true);
  });
}


export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable(TABLE_NAME);
}

