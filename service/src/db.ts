import knex from 'knex';
import _ from 'lodash';

const convertToCamel = (object: any) => {
  // Ignore INSERT results
  if(object.command === 'INSERT')
    return object;

  const result = Object.entries(object)
    .map(([key, value]) => [_.camelCase(key), value]);

  return Object.fromEntries(result);
};

export const DB = () => {
  return knex({
    client: "sqlite3",
    connection: {
      filename: "../dev.sqlite3"
    },

    wrapIdentifier: (value, origImpl) => {
      const snakeValue = _.snakeCase(value);
      return origImpl(snakeValue)
    },

    postProcessResponse: (result) => {
      let final: any;

      if (Array.isArray(result)) {
        final = result.map(row => convertToCamel(row));
      } else if(typeof result === 'object') {
        final = convertToCamel(result);
      } else {
        final = result;
      }

      return final;
    },
  });
};
