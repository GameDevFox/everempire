import { Response, Router } from "express";

import { DB, World } from "@everempire/common";

export const build = (db: DB) => {
  const worldTable = () => db<World>('world');

  const router = Router();

  router.get('/', async (_, res: Response<World[]>) => {
    const worlds = await worldTable().select();
    res.send(worlds);
  });

  router.post('/', async (req, res) => {
    const { name } = req.body;

    const playerCount = Math.floor(Math.random() * 1000);
    const world = { name, playerCount };

    const result = await worldTable()
      .insert(world, 'id');

    const [returning] = result;

    res.send(returning);
  });

  router.delete('/:id', async (req, res) => {
    const id = parseInt(req.params.id);

    await worldTable()
      .where({ id })
      .delete();

    res.send({ success: true });
  });

  return router;
};
