import 'source-map-support/register';

import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';

import { DB } from './db';
import { build as worldRouter } from './world-router';

const app = express();

app.use(cors());
app.use(bodyParser.json({ strict: false }));

const db = DB();

app.use('/world', worldRouter(db))

const port = 8080;

app.listen(port, () => {
  console.log(`Listening on port ${port} ...`);
});
